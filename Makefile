build:
	@cargo build

run:
	@cargo run

style-check:
	@rustup component add rustfmt 2> /dev/null
	cargo fmt --all -- --check

fmt:
	@rustup component add rustfmt 2> /dev/null
	cargo fmt --all --

lint:
	@rustup component add clippy 2> /dev/null
	cargo clippy --all-features --all -- -D clippy::all -D warnings

.PHONY: build run style-check fmt lint