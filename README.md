# Telegram blackbox bot

Telegram bot that allows you to organize anonymous feedback for your project.
User messages sent to the bot will be copied to administrators chat.
Administrators can reply to message.

## Quickstart
To run you should export environment variables:

 - `TELEGRAM_TOKEN` - telegram bot token
 - `TG_ADMINS_GROUP_ID` - telegram group id with admins (example: -100xxxx...)
 - `START_TEXT` - Message on receipt of the /start command
 - `SUCCESS_TEXT` - The text of the response to the user's message
 - `SUCCESS_ANSWER_TEXT` - Text of the message to the administrator about the successful response to the user's message
 - `STORAGE_FILE` - (optional) Path to sqlite file. If not specified, database in memory is used
 - `RUST_LOG` - (optional) Set to "debug" to see debug messages in console

### Option 1: Build crate

1. Install [Rust].
2. Setup your bot with [@botfather](https://t.me/botfather).
3. Build crate:
```console
cargo build
```
4. Start the bot:
```console
$ export TELEGRAM_TOKEN=123456789:blablabla
$ export TG_ADMINS_GROUP_ID=-1001234567890
$ export START_TEXT=Send any information anonymously
$ export SUCCESS_TEXT=Your message is accepted
$ export SUCCESS_ANSWER_TEXT=Reply sent
$ export STORAGE_FILE=storage.db
$ export RUST_LOG=debug
$ cargo run
```
5. Send `/start` to your telegram bot.

### Option 2: Run with Docker Compose

1. Install [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/compose-plugin/).
2. Setup your bot with [@botfather](https://t.me/botfather).
3. Copy `docker-compose.yml` to your device and edit the environment data to your requirements.
4. Run `docker compose up -d` to start the bot.
6. Send `/start` to your telegram bot.

[rust]: https://doc.rust-lang.org/cargo/getting-started/installation.html

## Donations

- [Telegram](https://t.me/iitc_news/39) (any bank card)
- [Bitcoin](https://blockchair.com/bitcoin/address/1FVqL2bUaw8J2rx6JDsHuj9643BDMHUCGf)
