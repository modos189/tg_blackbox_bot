use rusqlite::{params, Result};
use tokio_rusqlite::Connection;

#[derive(Clone, Debug)]
pub struct DbMessage {
    pub original_id: i32,
    pub forwarded_id: i32,
    pub user_id: u64,
}

pub(crate) async fn init(conn: Connection) -> Result<()> {
    conn.call(|conn| {
        conn.execute(
            "CREATE TABLE IF NOT EXISTS messages (
             original_id     INTEGER PRIMARY KEY,
             forwarded_id    INTEGER NOT NULL,
             user_id                 INTEGER NOT NULL
        )",
            [],
        )
    })
    .await?;
    Ok(())
}

pub(crate) async fn add_message(
    conn: Connection,
    original_id: i32,
    forwarded_id: i32,
    user_id: u64,
) -> Result<()> {
    conn.call(move |conn| {
        conn.execute(
            "INSERT OR IGNORE INTO messages (original_id, forwarded_id, user_id)
        values (?1, ?2, ?3)",
            params![original_id, forwarded_id, user_id],
        )
    })
    .await?;
    Ok(())
}

pub(crate) async fn get_message_by_forwarded(
    conn: Connection,
    forwarded_id: i32,
) -> Result<DbMessage> {
    conn.call(move |conn| {
        let mut stmt = conn.prepare("SELECT * FROM messages WHERE forwarded_id = ?1 LIMIT 1")?;
        stmt.query_row(params![forwarded_id], |row| {
            Ok(DbMessage {
                original_id: row.get(0)?,
                forwarded_id: row.get(1)?,
                user_id: row.get(2)?,
            })
        })
    })
    .await
}

pub(crate) async fn get_message_by_original(
    conn: Connection,
    original_id: i32,
) -> Result<DbMessage> {
    conn.call(move |conn| {
        let mut stmt = conn.prepare("SELECT * FROM messages WHERE original_id = ?1 LIMIT 1")?;
        stmt.query_row(params![original_id], |row| {
            Ok(DbMessage {
                original_id: row.get(0)?,
                forwarded_id: row.get(1)?,
                user_id: row.get(2)?,
            })
        })
    })
    .await
}
