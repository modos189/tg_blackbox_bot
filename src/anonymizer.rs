use teloxide::{prelude::*, types::UserId};

use crate::{database, ConfigParameters};

pub(crate) async fn start(
    bot: AutoSend<Bot>,
    msg: Message,
    cfg: ConfigParameters,
) -> Result<(), teloxide::RequestError> {
    bot.send_message(msg.chat.id, cfg.start_text).await?;
    Ok(())
}

pub(crate) async fn reply(
    bot: AutoSend<Bot>,
    msg: Message,
    cfg: ConfigParameters,
) -> Result<(), teloxide::RequestError> {
    match msg.reply_to_message() {
        Some(forwarded) => {
            if let Ok(original_message) =
                database::get_message_by_forwarded(cfg.conn, forwarded.id).await
            {
                bot.copy_message(UserId(original_message.user_id), msg.chat.id, msg.id)
                    .reply_to_message_id(original_message.original_id)
                    .allow_sending_without_reply(true)
                    .await?;
                bot.send_message(msg.chat.id, cfg.success_answer_text)
                    .reply_to_message_id(msg.id)
                    .await?;
            }
        }
        None => {}
    };

    Ok(())
}

pub(crate) async fn forward(
    bot: AutoSend<Bot>,
    msg: Message,
    cfg: ConfigParameters,
) -> Result<(), teloxide::RequestError> {
    let forwarded_id = match msg.reply_to_message() {
        Some(original) => {
            match database::get_message_by_original(cfg.conn.clone(), original.id).await {
                Ok(forwarded_message) => {
                    bot.copy_message(cfg.admins_group_id, msg.chat.id, msg.id)
                        .reply_to_message_id(forwarded_message.forwarded_id)
                        .allow_sending_without_reply(true)
                        .await?
                        .message_id
                }
                _ => {
                    bot.copy_message(cfg.admins_group_id, msg.chat.id, msg.id)
                        .await?
                        .message_id
                }
            }
        }
        None => {
            bot.copy_message(cfg.admins_group_id, msg.chat.id, msg.id)
                .await?
                .message_id
        }
    };

    let UserId(user_id) = match msg.from() {
        Some(user) => user.id,
        _ => {
            error!("Failed to get user ID for resend");
            return Ok(());
        }
    };

    match database::add_message(cfg.conn, msg.id, forwarded_id, user_id).await {
        Ok(()) => {}
        _ => {
            error!("Error of writing a resent message to DB");
            return Ok(());
        }
    }

    bot.send_message(msg.chat.id, cfg.success_text)
        .reply_to_message_id(msg.id)
        .await?;

    Ok(())
}
