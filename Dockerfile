# Builder
FROM ekidd/rust-musl-builder:latest AS builder

# We'll get to what this file is below!
COPY --chown=rust:rust dummy.rs .
# If this changed likely the Cargo.toml changed so lets trigger the recopying of it anyways
COPY --chown=rust:rust Cargo.toml .
# We'll get to what this substitution is for but replace main.rs with lib.rs if this is a library
RUN sed -i 's@src/main.rs@dummy.rs@' Cargo.toml
# Drop release if you want debug builds. This step cache's our deps!
RUN cargo build --release
# Now return the file back to normal
RUN sed -i 's@dummy.rs@src/main.rs@' Cargo.toml
# Copy the rest of the files into the container
COPY --chown=rust:rust . .
# Now this only builds our changes to things like src
RUN cargo build --release

# Final image
FROM scratch

WORKDIR /
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/tg_blackbox_bot /

USER rust

ENV RUST_BACKTRACE=1
ENTRYPOINT ["/tg_blackbox_bot"]
